Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0027-ewe-10-uv-laser).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         |  [de](https://gitlab.com/ourplant.net/products/s3-0027-ewe-10-uv-laser/-/raw/main/01_operating_manual/S3-0027_B2_BA_Einzelwafereinzug.pdf)                   |
| assembly drawing         |[de](https://gitlab.com/ourplant.net/products/s3-0027-ewe-10-uv-laser/-/raw/main/02_assembly_drawing/s3-0027_B_ZNB_ewe-1_0-uv-laser.pdf)     |
| circuit diagram          |[de](https://gitlab.com/ourplant.net/products/s3-0027-ewe-10-uv-laser/-/raw/main/03_circuit_diagram/S3-0027-EPLAN-A.pdf)                  |
| maintenance instructions |[de](https://gitlab.com/ourplant.net/products/s3-0027-ewe-10-uv-laser/-/raw/main/04_maintenance_instructions/S3-0027_A_Wartungsanweisungen.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0027-ewe-10-uv-laser/-/raw/main/04_maintenance_instructions/S3-0027_A_Maintenance_instructions.pdf)                    |
| spare parts              |[de](https://gitlab.com/ourplant.net/products/s3-0027-ewe-10-uv-laser/-/raw/main/05_spare_parts/S3-0027_A2_EVL.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0027-ewe-10-uv-laser/-/raw/main/05_spare_parts/S3-0027_A2_EVL_engl.pdf)                    |

